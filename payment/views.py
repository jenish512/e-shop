from django.shortcuts import render, redirect, get_object_or_404
import braintree
from django.conf import settings
from orders.models import Order
from .tasks import payment_completed
# Create your views here.

# instantiate Braintree payment gateway
gateway = braintree.BraintreeGateway(config=settings.BRAINTREE_CONF)

def payment_process(request):
    order_id = request.session.get('order_id')
    order = get_object_or_404(Order, id=order_id)
    total_cost = order.get_total_cost()

    if request.method == "POST":
        # retrieve nonce
        nonce = request.POST.get('payment_method_nonce', None)
        # create and submit transaction
        result = gateway.transaction.sale({
            'amount': f'{total_cost:.2f}',
            'payment_method_nonce': nonce,
            'options': {
                'submit_for_settlement': True
            }
        })

        if result.is_success:
            # mark the order paid
            order.paid = True
            # store the unique transaction id
            order.braintree_id = result.transaction.id
            order.save()
            # launch asynchronous task 
            payment_completed.delay(order.id)
            return redirect('payment:done')
        else:
            
            err_list = []
            for error in result.errors.deep_errors:
                err_list.append(f'{error.code}: {error.message}')

            err_text = '\n'.join(err_list)
            request.session['err_text'] = err_text
            return redirect('payment:canceled')

    else:
        # generate token
        client_token = gateway.client_token.generate()
        return render(request, 'payment/process.html',
                {
                    'order': order,
                    'client_token' : client_token
                }
            )
    
def payment_done(request):
    return render(request, 'payment/done.html')
    
def payment_canceled(request):
    # return render(request, 'payment/canceled.html')
    # err_code = request.session.get('err_code')
    err_text = request.session.get('err_text')
    return render(request, 'payment/canceled.html',
                                    {
                                        # 'err_code': err_code,
                                        'err_text': err_text,
                                    }
                )